# ESP32S3 Camera Servo Problem

This is a minimal documentation on an attempt to combine working ESP32S3 camera and servo control capabilities. If you have read this already and want to get the final working code, use the list below.

- [Camera Example](./Examples/ServoExample/)
- [Servo Example](./Examples/CameraWebServer/)
- [Combined Example](./Examples/CombinedExample/)

The plan is to do the following. This is also the index of this document.

- [Install ESP32 Board Definitions](#installing-esp32-board-definitions)
- [Working ESP32S3 Camera Example](#working-esp32s3-camera-example)
- [Working ESP32S3 Servo Example](#working-esp32s3-servo-example)
- [Combined Camera Servo Example (TLDR Version)](#combined-camera-servo-example-tldr-version)
- [Combined Camera Servo Example (Extended Version)](#combined-camera-servo-example-extended-version)
- [Conclusions](#conclusions)

## Installing ESP32 Board Definitions

First, look at the Seeed Studio [ESP32S3 Getting Started](https://wiki.seeedstudio.com/xiao_esp32s3_getting_started/) page. What we are looking for are the instructions on what libraries to install in order to get any code compiling on the board. The docs are a bit outdated, here is an updated version as of 5 June 2024.

First things first, go to **File > Preferences** and add the following URL to **Additional Boards Manager URLs**.

```
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
```

Then, go to **Tools > Board > Boards Manager** and look for **esp32**. You should be able to find the **esp32** definition.

![Boards Manager](./Images/BoardsManager.png)

## Working ESP32S3 Camera Example

The instructions regarding setting up camera on hardware level are fine. There are a few problems with the example code. 

First, grab the the original [CameraWebServer](https://github.com/limengdu/SeeedStudio-XIAO-ESP32S3-Sense-camera/tree/main/CameraWebServer) example code. Make sure that you open it as a project as there are additional files that should be compiled with the main **CameraWebServer** file.

![Camera Project](./Images/CameraProject.jpg)

When you first compile it, you are very likely to get the following errors.

```
app_httpd.cpp: In function 'void setupLedFlash(int)':
app_httpd.cpp:1394:6: error: 'ledcSetup' was not declared in this scope; did you mean 'ledc_stop'?
 1394 |      ledcSetup(LED_LEDC_CHANNEL, 5000, 8);
      |      ^~~~~~~~~
      |      ledc_stop
app_httpd.cpp:1395:6: error: 'ledcAttachPin' was not declared in this scope; did you mean 'ledcAttach'?
 1395 |      ledcAttachPin(pin, LED_LEDC_CHANNEL);
      |      ^~~~~~~~~~~~~
      |      ledcAttach
exit status 1
'ledcSetup' was not declared in this scope; did you mean 'ledc_stop'?
```

In order to fix this, you need to open the `app_httpd.cpp` file and replace `ledcSetup` and `ledcAttachPin` calls with a single `ledcAttach` call. This is due to recent changes in the Arduino `LEDC` api, described more in detail [here](https://github.com/espressif/arduino-esp32/blob/master/docs/en/migration_guides/2.x_to_3.0.rst#ledc) and [here}(https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/api/ledc.html).

Use **Edit > Find** to look for the line `ledcSetup(LED_LEDC_CHANNEL, 5000, 8);`. Comment the following lines, replace them with a single `ledcAttach` function call.

```
// ledcSetup(LED_LEDC_CHANNEL, 5000, 8);
// ledcAttachPin(pin, LED_LEDC_CHANNEL);
ledcAttach(pin, 5000, 8);
```

![Camera Error Fix](./Images/CameraErrorFix.jpg)

This should get the camera code compiling. You should be able to see your ESP32 board connecting to the network and giving you an IP address via Arduino **Serial Monitor**.

![Camera Serial Output](./Images/CameraSerial.jpg)

In the browser you should be able to access a demo interface where you can play around with a variety of camera parameters. To access the video stream, you need to click on **Start Stream**.

![Camera Interface](./Images/CameraInterface.jpg)

## Working ESP32S3 Servo Example

Driving a hobby servo such as the [DFRobot 9g 180° MicroServo](https://www.dfrobot.com/product-255.html) should be fairly easy. These can be powered with 5V and it seems that they accept 0-3.3V as logic levels for PWM control. 

SparkFun has a rather informative [hobby servo tutorial](https://learn.sparkfun.com/tutorials/hobby-servo-tutorial/all) that was very useful to confirm servo colored wire mappings.

- Red: XIAO 5V power
- Brown: XIAO Ground
- Orange: D0 pin for control signal

I also used a logic analyzer to see if a pin is sending the expected PWM signal. The screenshot below is how it should look like.

![Servo Logic](./Images/ServoLogic.jpg)

Several libraries were tried, but the one that did work eventually was **ESP32Servo**. One can install it via the Arduino Library Manager, **Tools > Manage Libraries**. 

![Servo Library](./Images/ServoLibrary.jpg)

The following (modified) example code was used. The original can be found [here](https://forum.arduino.cc/t/esp32-feather-s3-servo/1010033).

```
#include <ESP32Servo.h>
int servo_pin = D0;
Servo myservo;

void setup() {
  Serial.begin(115200);
  delay(500);
  Serial.println("\nServo sweep and LED blink");
  myservo.attach(servo_pin);
  myservo.write(0);
}

void loop() {
  int pos;
  Serial.println("loop(): on");
  for (pos = 0; pos <= 180; pos += 2) {
    myservo.write(pos);
    delay(15);
  }
  Serial.println("loop(): off");
  for (pos = 180; pos >= 0; pos -= 2) {
    myservo.write(pos);
    delay(15);
  }
}
```

It should work like this.

![Servo Working](./Images/ServoWorking.mp4)

## Combined Camera Servo Example (TLDR Version)

To be done...

## Combined Camera Servo Example (Extended Version)

The starting point here are the two working examples above. The hypothesis is that if one will find the place where user interface elements are described in the camera example, it would be fairly easy to add another toggle to trigger the servo. One would take the camera example as the starting point and copy over servo init code first. The servo on and servo off code can be made into functions that can be triggered the same way any of the other functions in the camera example.

I started by copying the [CameraWebServer](./Examples/CameraWebServer) example and renaming it to [CombinedExample](./Examples/CombinedExample). Important is to rename the main source file as well, in this case `CameraWebServer.ino` to `CombinedExample.ino`. What is also important is that you copy over all the supporting files from the `CameraWebServer` example too. The directory structure should look as below.

```
CombinedExample
├── app_httpd.cpp
├── camera_index.h
├── camera_pins.h
└── CombinedExample.ino
```

Try to compile to see if it still works. It does! 

```
........................
WiFi connected
Camera Ready! Use 'http://193.167.5.150' to connect
```

Next step is to copy over servo init code and see how that behaves. First, the includes...

```
#include <ESP32Servo.h>
```

Then the servo variables.

```
int servo_pin = D0;
Servo myservo;
```

Then servo init code in the `setup()` after initializing the serial connection.

```
myservo.attach(servo_pin);
myservo.write(0);
```

And then, add the remaining servo code to the `loop()` function. Since the loop function contains only a `delay()` call, I will replace that with the servo code entirely.

```
void loop() {
  int pos;
  Serial.println("loop(): on");

  for (pos = 0; pos <= 180; pos += 2) {
    myservo.write(pos);
    delay(15);
  }

  Serial.println("loop(): off");
  for (pos = 180; pos >= 0; pos -= 2) {
    myservo.write(pos);
    delay(15);
  }
}
```

Does it compile and upload? Yes, but... The servo started spinning 360 degrees and the pitch of the sound it emmited was getting higher and higher. I was worried if the servo is still allright and uploaded the working servo code again to see. The servo still worked as expected.

> There is a problem, but it is not clear what is causing it.

It is possible that the camera web server example is doing something with the pin that I am using to control the servo. Let's look for hints supporting this.

In the camera example there is a part where the camera settings are defined. 

```
camera_config_t config;
config.ledc_channel = LEDC_CHANNEL_0;
config.ledc_timer = LEDC_TIMER_0;
...
```

One can see that some LEDC API related things are going on there. The ESP32Servo library also uses it. I decided to take a look at the [ESP32PWM.h](https://github.com/madhephaestus/ESP32Servo/blob/master/src/ESP32PWM.h) file in the library itself. I noticed the following line.

```
int pwmChannel = 0;                         // channel number for this servo
```

it seems that the camera and the servo are using the same channel for PWM control. To reduce confusion in case someone else reads this, I decided to change the channel number in the code of the combined example like so. Changing `LEDC_CHANNEL_0` to `LEDC_CHANNEL_1`.

```
camera_config_t config;
config.ledc_channel = LEDC_CHANNEL_1;
config.ledc_timer = LEDC_TIMER_0;
```

Does it work? No. The servo does move, but not in a way that is expected. Let's try to look further. How about we change the `LEDC_TIMER_0` to `LEDC_TIMER_1` as well?

```
config.ledc_timer = LEDC_TIMER_1;
```

Let's try now. Getting tiny movements, but not what expected. How about moving the servo init code after the camera setup? Changing back the `LEDC` settings and moving the servo code just before WiFi setup. 

> It works!

The exact cause of the problem is unknown, but it has something to do with what is happening during the camera setup. The camera setup overrides things that the ESP32Servo library is trying to do.

Next step is to make it spin when user clicks a button in the web interface. In the camera example code it is said that all good things are going on in the web server process.

```
// Do nothing. Everything is done in another task by the web server
delay(10000);
```

Let's take a closer look at the web server process. I assume that it is the [app_httpd.cpp](./Examples/CombinedExample/app_httpd.cpp) file. It looks like there are endpoints defined in the `startCameraServer()` function definition.

```
httpd_uri_t index_uri = {
        .uri = "/",
        .method = HTTP_GET,
        .handler = index_handler,
        .user_ctx = NULL
    };
```

Here the endpoint being `/` or root. Each enpoint has its own handler, in this case it is `index_handler`. For `/status` it is `status_handler`. `index_handler` and `status_handler` are references to functions defined somewhere there in the code. Let's take a closer look.

The following is the code of the `index_handler`.

```
static esp_err_t index_handler(httpd_req_t *req)
{
    httpd_resp_set_type(req, "text/html");
    httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
    sensor_t *s = esp_camera_sensor_get();
    if (s != NULL) {
        if (s->id.PID == OV3660_PID) {
            return httpd_resp_send(req, (const char *)index_ov3660_html_gz, index_ov3660_html_gz_len);
        } else if (s->id.PID == OV5640_PID) {
            return httpd_resp_send(req, (const char *)index_ov5640_html_gz, index_ov5640_html_gz_len);
        } else {
            return httpd_resp_send(req, (const char *)index_ov2640_html_gz, index_ov2640_html_gz_len);
        }
    } else {
        log_e("Camera sensor not found");
        return httpd_resp_send_500(req);
    }
}
```

As we can see, it accepts a pointer to a request parameter `*req` with the type `httpd_req_t`. Then a set of functions is used to specify the HTTP return type and content. I believe we could achieve motor movement by adding two new handlers: one for on, another for off. The one below is for turning it on. It is very similar for the off functionality.

```
static esp_err_t servo_on_handler(httpd_req_t *req)
{
  httpd_resp_set_type(req, "text/html");
  httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
    
  for (int pos = 0; pos <= 180; pos += 2) {
    myservo.write(pos);
    delay(15);
  }

  const char resp[] = "Servo on";
  httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
  return ESP_OK;
}
```

I borrowed code from [ESP32 HTTP Server documentation](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-reference/protocols/esp_http_server.html) for the response part. Nothing complicated has to be returned when turning the servos on and off.

Now we need to define the endpoints. A good idea is to try to compile the code at this point. It does not, because some of the global variables, such as the `myservo` are not defined yet. Let's try to transfer that part from the main code.

Starting with `#include <ESP32Servo.h>`. Then the servo variables.

```
int servo_pin = D0;
Servo myservo;
```

Then, servo init code that I will put into the `startCameraServer()` function definition.

```
// Servo init
myservo.attach(servo_pin);
myservo.write(0);
```

I added this at the end of the function. Let's try to compile it now. It does not as I have to remove the code in the `loop()` function so it looks like the following.

```
// Do nothing. Everything is done in another task by the web server
delay(10000);
```

It compiles. Let's try to upload. It does upload and the servo is reset to its initial position as instructed by the servo init code part.

Now we need the endpoints. 

```
httpd_uri_t servo_on_uri = {
    .uri = "/servo_on",
    .method = HTTP_GET,
    .handler = servo_on_handler,
    .user_ctx = NULL
};
```

This and the respective one for servo off functionality we add after all the other ones in the `startCameraServer()` code. They also need to be registered using the `httpd_register_uri_handler` function.

```
httpd_register_uri_handler(camera_httpd, &servo_on_uri);
httpd_register_uri_handler(camera_httpd, &servo_off_uri);
```

> Compiling. Uploading. Testing. It works!

![Servo API Works](./Images/ServoAPIWorks.mp4)

Next, look for the HTML part and add UI to do that on click. Let's follow the `index_handler` and see where it gets the HTML content for the response from. It seems that it gets the data from a set of functions: `index_ov3660_html_gz`, `index_ov5640_html_gz` and `index_ov2640_html_gz`.

```
if (s->id.PID == OV3660_PID) {
    return httpd_resp_send(req, (const char *)index_ov3660_html_gz, index_ov3660_html_gz_len);
} else if (s->id.PID == OV5640_PID) {
    return httpd_resp_send(req, (const char *)index_ov5640_html_gz, index_ov5640_html_gz_len);
} else {
    return httpd_resp_send(req, (const char *)index_ov2640_html_gz, index_ov2640_html_gz_len);
}
```

It looks like they are build into the ESP32 core, but we can easily roll our own! Let's start with a very basic HTML output with two hyperlinks. I had to modify the `index_handler` to make it work as follows.

```
static esp_err_t index_handler(httpd_req_t *req)
{
  httpd_resp_set_type(req, "text/html");
  sensor_t *s = esp_camera_sensor_get();
  if (s != NULL) {
    const char servo_index_html[] = "<a href=\"/servo_on\">on</a> | <a href=\"/servo_off\">off</a>";
    httpd_resp_send(req, servo_index_html, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
  } else {
    log_e("Camera sensor not found");
    return httpd_resp_send_500(req);
  }
}

```

And it works! But in a non-dynamic manner. Meaning that it redirects to an empty page and it would require some JavaScript code to make it work properly. Aother thing is to get the camera view integrated there. Maybe it is time to consult the [ESP32 HTTP server](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-reference/protocols/esp_http_server.html) documentation to see how we could reuse some existing code...

I got back to looking at the `index_ov3660_html_gz`, `index_ov5640_html_gz` and `index_ov2640_html_gz` variables. It appears they are defined in [camera_index.h](./Examples/CombinedExample/camera_index.h) as gzip-compressed stream of bytes. Maybe a good starting point could be loading them in the browser and then copying source to make it editable. It could be then gzpiped again using a script or another suitable method.

Loading the index page and copying its source worked. It resulted into [index.html](./Examples/CombinedExample/index.html) file that can now be modified to do what we want. I will try to add a toggle switch there, just above all other controls. 

I am trying to add a new toggle switch which seems to be identified as `slider` in existing code.

```
<div class="input-group" id="servo-group">
    <label for="servo">Servo On/Off</label>
    <div class="switch">
        <input id="servo" type="checkbox" class="default-action" checked="checked">
        <label class="slider" for="servo"></label>
    </div>
</div>
```

In order to create a compressed C array format output, one needs to go through two-step process using two command line tools.

```
gzip -c index.html > index.html.gz
xxd -i index.html.gz > index_html_gz.c
```

The two commands above will create a file `index_html_gz.c` and it will be possible to copy its contents to our `camera_index.h`. Below a fragment of the resulting output. It also creates a variable with the length of the array that we can now use in our code.

```
unsigned char index_html_gz[] = {
  0x1f, 0x8b, 0x08, 0x08, 0x06, 0xa0, 0x61, ... );
unsigned int index_html_gz_len = 6835;
```

In fact, one can modify the second step to replace the `camera_index.h` file.

```
xxd -i index.html.gz > camera_index.h
```

Compile, upload, access the web interface and yes, it is there.

![Combined Custom UI Element](./Images/CombinedCustomUIElement.jpg)

Next step is to call the servo on and off endpoints once the toggle switch is flipped. Down below in the HTML code one can find the `<script>` section, this is where the JavaScript lives. It seems that all interface elements have an `onchange` handler that checks the current state of the UI element and triggers actions accordingly.

In the case of the servos, we need to define `servoOn` and `servoOff` functions that could be then called once the value of the servo UI switch changes. Below is how the `servoOn` function looks like.

```
const servoOn = () => {
  const query = `${baseHost}/servo_on`
  fetch(query)
    .then(response => {
      console.log(`request to ${query} finished, status: ${response.status}`)
    })
}
```

Then, we need the `onchange` handler. What we do in the code below is we find the `servo` UI element using its ID and then create an `onchange` event handler.

```
const servo = document.getElementById('servo')
servo.onchange = () => {
  servo.checked ? servoOn() : servoOff()
}
```

> It works!

![Combined UI to Servo](./Images/CombinedUIToServo.mp4)

The final thing is to enable video stream when the page just opens. To do that, we look for the **Start Stream** button functionality. It looks like this HTML element over here.

```
<button id="toggle-stream">Start Stream</button>
```

The script part tells us that it could be that all we have to do is to change `Start Stream` to `Stop Stream`, judging by the following code.

```
const streamButton = document.getElementById('toggle-stream')
...
streamButton.onclick = () => {
  const streamEnabled = streamButton.innerHTML === 'Stop Stream'
  if (streamEnabled) {
    stopStream()
  } else {
    startStream()
  }
}

```

It also might be that we need to call `startStream()` when the page loads. It may be a more sensible solution. The function itself looks like this.

```
const startStream = () => {
  view.src = `${streamUrl}/stream`
  show(viewContainer)
  streamButton.innerHTML = 'Stop Stream'
}
```

It does not seem that it is triggered anywhere besides from within the `streamButton.onclick` handler function. So let's look up our `onLoad` function and it seems that the following is a good candidate.

```
document.addEventListener('DOMContentLoaded', function (event) { ...
```

Let's try to put the `startStream()` call in there, right after the `stopStream` and `startStream` definitions. 

```
const stopStream = () => {
  window.stop();
  streamButton.innerHTML = 'Start Stream'
}

const startStream = () => {
  view.src = `${streamUrl}/stream`
  show(viewContainer)
  streamButton.innerHTML = 'Stop Stream'
}

startStream()
```

Let's repeat the gzip process and try to compile and run.

> Problem Solved!

It works. One can flip the switch and see the servo spinning in the webcam.

![Problem Solved Video](./Images/ProblemSolved.mp4)

## Conclusions

It is possible, but requires understanding about many separate things. In a way it is the good old regular frontend / backend development, but the frontend part is spiced up with the need for some extra tooling such as `gzip` and `xxc`. On the backend part one needs to familiarize with the ESP32 web server internals as well as what the Arduino `LEDC` API has to offer.

Finally, from here it should be possible to modify the `index.html` to look like anything you want and add as many additional features on the backend side. There seems to be no limit in terms of what can be achieved. The original examples are loaded with a hefty amount of features that you may use, so the bulk of the work is actually cleaning up to create something simple and robust by deleting everything you do not need.