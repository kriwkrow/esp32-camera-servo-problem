#include <ESP32Servo.h>

int servo_pin = D0;

Servo myservo;

void setup() {
  Serial.begin(115200);
  delay(500);
  Serial.println("\nServo sweep and LED blink");

  //  pinMode(servo_pin, OUTPUT);
  //  digitalWrite(servo_pin, LOW);

  //  myservo.setPeriodHertz(50);
  //  myservo.attach(servo_pin, 500, 2400);
  myservo.attach(servo_pin);
  myservo.write(0);
}

void loop() {
  int pos;
  Serial.println("loop(): on");

  for (pos = 0; pos <= 180; pos += 2) {
    myservo.write(pos);
    delay(15);
  }

  Serial.println("loop(): off");
  for (pos = 180; pos >= 0; pos -= 2) {
    myservo.write(pos);
    delay(15);
  }
}
